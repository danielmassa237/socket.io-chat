import "reflect-metadata";
import { Container } from "inversify";
import { interfaces, InversifyExpressServer, TYPE } from "inversify-express-utils";
import { join as pathJoin } from "path";
import { static as expressStatic, Request, Response, NextFunction } from "express";
import { urlencoded, json } from "body-parser";
import * as logger from "morgan";

import TAGS from "./controller/tags";
import { HomeController } from "./controller/home";
import { SocketService } from "./service/socket";

// Load everything neeede to the Container
const container = new Container();

container.bind<interfaces.Controller>(TYPE.Controller).to(HomeController).whenTargetNamed(TAGS.HomeController);

// Start the server
const inversifyExpressServer = new InversifyExpressServer(container);
inversifyExpressServer.setConfig(app => {
	// Not sure why these are needed
	app.use(json());
	app.use(urlencoded({
		extended: true
	}));

	app.use(logger("dev"));
	app.use(expressStatic(pathJoin(__dirname, "view")));
	app.use(expressStatic(pathJoin(process.cwd(), "/public")));
});

inversifyExpressServer.setErrorConfig(app => {
	app.use((err: any, req: Request, res: Response, next: NextFunction) => {
        res.status(err.status || 500);
        res.send(err);
	});
});

const port: number = Number(process.env.PORT) || 3000;
const app = inversifyExpressServer.build();
const server = app.listen(port);
const socket = new SocketService(server);

console.log(`server started on port ${port}`);