const TYPES = {
	SocketService: Symbol("SocketService"),
	ServerInstance: Symbol("ServerInstance")
};

export default TYPES;