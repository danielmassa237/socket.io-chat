import { Server } from "http";
import * as socketIO from "socket.io";
import * as redis from "socket.io-redis";

export class SocketService {

	private server: SocketIO.Server;

	constructor(server: Server) {
		// Create socket.io server
		this.server = socketIO(server);

		this.addRedisAdapter();

		this.connection();
	}

	private addRedisAdapter() {
		this.server.adapter(redis({
			host: "127.0.0.1",
			port: 6379
		}));
	}

	private connection() {

		const io = this.server.of("mynamespace");

		// this.server.on("connection", client => {
		// 	console.log("a user connected");

		// 	// Success!  Now listen to messages to be received
		// 	client.on("chat message", msg => {
		// 		console.log("message: " + msg);
		// 		this.server.emit("chat message", msg, {
		// 			for: "everyone"
		// 		});
		// 	});

		// 	client.on("disconnect", msg => {
		// 		console.log("user disconnected", msg);
		// 	});
		// });
		io.on("connection", socket => {

			socket.on("message-all", data => {
				io.emit("message-all", data);
			});

			socket.on("join", (room) => {
				socket.join(room);
				io.emit("message-all", `Socket ${socket.id} joined to room ${room}`);
			});

			socket.on("message-room", data => {
				const room = data.room;
				const message = data.message;
				io.to(room).emit("message-room", data);
			});

			socket.on("disconnect", msg => {
				console.log("user disconnected", msg);
			});

		});

	}
}
