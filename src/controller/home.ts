import { controller, httpGet } from "inversify-express-utils";
import { injectable } from "inversify";
import { Request, Response } from "express";

@injectable()
@controller("/")
export class HomeController {

	@httpGet("/")
	public async get(req: Request, res: Response) {
		res.sendFile("/index.html");
	}
}